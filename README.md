# dashboard_nosocomial-diseases

Dashboard presenting the data from data.gouv of the prevention of nosocomial diseases in the French health establishments from 2007 to 2009.

The dashboard was deployed online and can be accessed at the following link : https://celie-dasilva-dashboard.herokuapp.com/

Because of the size of the datasets used, an error can occur when trying to open the app. Please, refresh the page until you visualize the home page of the app. 

It is recommanded to visualize the app using Google Chrome.

# Dashboard presentation

The dashboard is composed of three different pages : 
- **Home** : presentation of nosocomial diseases, presentation of the datasets, access to the data
- **Global overview** : evolution of the establishments' classes and indicators from 2007 to 2009
- **Overview by year** : year selection, repartition of the classes of establishment in France, marks repartition by criteria, surgery assessment

The navigation between the pages is possible through the buttons in the left frame of the dashboard.
