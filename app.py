### Modules importation
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State #Modules pour récupérer les informations 
import dash_bootstrap_components as dbc
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import plotly.express as px
import xlrd
import openpyxl
from pyproj import Transformer
from dash_bootstrap_templates import load_figure_template
from dash import dash_table
import plotly.graph_objects as go


### Dataset importation and processing
folder_path = ""
fname = os.path.join(folder_path, "data_maladies_nosocomiales_cleaned.csv")
df = pd.read_csv(fname, sep=";")
print(df.columns)
df["annee"] = df["annee"].apply(str)
df["enquete_SURVISO"] = df["enquete_SURVISO"].apply(str) #Conversion of the year from int to str.

print(df.head())
print(df.columns)
print(df.dtypes)



### .csv summarizing the assessment criteria definition
fname_classe = os.path.join(folder_path, "classes_definition.csv")
df_classe = pd.read_csv(fname_classe, sep=";")

df_classe.head()



### Different functions for the dashboard
def newLegend(fig, newNames):
	"""Updates legend names in figures"""
	for item in newNames:
		for i, elem in enumerate(fig.data[0].labels):
			if elem == item:
				fig.data[0].labels[i] = newNames[item]
	return(fig)

## Link CSS sheet
bootstrap_css = folder_path +"/assets/bootstrap.css"
custom_css = folder_path +"/assets/custom.css"

### Generation of the dashboard named "app" 
app = dash.Dash(__name__, suppress_callback_exceptions=True) 
server = app.server #Allow Heroku to recognize and connect the server
## By default, load the CSS in the assets file. 
## CSS downloaded from BOOTSTRAP : SANDSTONE theme

### Styling the Sidebar
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "20rem",
    "padding": "2rem 1rem",
    "background-color": "#F8F5F0",
    "overflow": "scroll"
}


### Styling the content

CONTENT_STYLE = {
    "margin-left": "24rem",
    "margin-right": "8rem",
    "padding": "2rem 1rem",
}


sidebar = html.Div(
    [
        html.H6("Hospital-acquired infection in France", className="display-4", style={"text-align":"center"}),
        html.Br(),
        html.Hr(),
        html.Br(),
        html.P(
            "Overview of the measures taken for the prevention of nosocomial diseases in French establishments between 2007 and 2009", 
            className="lead", style = {"text-align":"center"}
        ),
        html.Br(),
        dbc.Nav(
            [
                dbc.NavLink("Home", href="/", active="exact", className="nav-pills-customized"),
                dbc.NavLink("Global Overview", href="/global", active="exact", className="nav-pills-customized"),
                dbc.NavLink("Overview By Year", href="/by-year", active="exact", className="nav-pills-customized"),
            ],
            vertical=True,
            pills=True,
            fill = True,
            className="nav-pills-customized"
        ),
    ],
    style=SIDEBAR_STYLE,
)

content = html.Div(id="page-content", children=[], style=CONTENT_STYLE)

app.layout = html.Div([
		dcc.Location(id="url"),
   		sidebar,
    	content
    ])


###########################################################################################################################
########################################## Figure for first tab of the dashboard ##########################################
###########################################################################################################################


	#Plot for id = nb_assess
bar_nb_estab = px.bar(df,  
					x = [year for year in df.annee.unique()], #Retrieve the different years
			        y = [len(df.loc[df['annee'].isin([year])]) for year in df.annee.unique()],#Count the number of line for each year (1 line = 1 establisment)
			        range_y =  [2500,2800], #display y abscisse from 2500 to 2800
			        labels = {'x': "Years", 'y': "Number of establishments"},
			        color = df.annee.unique())

bar_nb_estab.update_layout(showlegend= False)

#Plot for SURVISO 

count_surviso = df.groupby(['annee', 'enquete_SURVISO']).size() #Group by year an class and count number of each in class for each year
surviso_frame = count_surviso.to_frame(name = 'surviso_nb').reset_index() #Conversion of the series to a dataframe : colnames = | annee | enquete_SURVISO | surviso_nb |
surviso_frame.head()

surviso_graph = px.line(surviso_frame,
					x = "annee",
					y = 'surviso_nb',
					color = 'enquete_SURVISO',
					markers=True,
					title = 'Evolution of the number of SURVISO surveys carried out',
					labels = {'annee': "Years", 'surviso_nb': "SURVISO Surveys"})
surviso_graph.update_layout(legend_title = "Status")
newnames = {"1" : "Assessment carried out", "0":"Assessment not carried out","2": "Establishment not concerned"}
surviso_graph.for_each_trace(lambda t: t.update(name = newnames[t.name],
                                      legendgroup = newnames[t.name],
                                      hovertemplate = t.hovertemplate.replace(t.name, newnames[t.name])
                                     )
                  )

	#Plot for proportion of services assessed by SURVISO

count_surviso_prop = df.groupby(['annee','enquete_SURVISO', 'part_service_chi_SURVISO']).size() #Group by year an class and count number of each in class for each year
surviso_prop_frame = count_surviso_prop.to_frame(name = 'nb').reset_index() #Conversion of the series to a dataframe : colnames = | annee | enquete_SURVISO | surviso_nb |

surviso1_prop_frame = surviso_prop_frame[surviso_prop_frame.enquete_SURVISO =="1"] #Selection of the establissment who assessed SURVISO
surviso1_prop_frame.head()

prop_graph = px.scatter(surviso1_prop_frame, 
				x = surviso1_prop_frame.part_service_chi_SURVISO,
				y = surviso1_prop_frame.nb,
				labels = {'part_service_chi_SURVISO': "Proportions", "nb":"Number"},
				size = "nb",
				color= "annee")
prop_graph.update_layout(legend_title = "Years" )

###########################################################################################################################
########################################## Data and figure for second tab of the dashboard ################################
###########################################################################################################################

#Data required for the map
fname_finess = os.path.join(folder_path, "t-finess_modified.xlsx")
df_finess = pd.read_excel(fname_finess,engine='openpyxl') #Opening the t-finess file : gave the geographic coordinates of the establissement base on finess number
print(df_finess.head())

df_merged = pd.merge(df,df_finess[['finess', 'geoloc_x', 'geoloc_y']], on = "finess") #Merging the two datasets

#Conversion of the coordinates from Lambert 93 to WGS84
transformer = Transformer.from_crs("epsg:2154", "epsg:4326") #2154 = Lambert 93 ; 4326 = most common representation for online map

def convertCoords(row):
	"""Converts the coordinates in Lambert 93 to WGS84"""
	x,y = transformer.transform(row['geoloc_x'],row['geoloc_y'])
	return pd.Series([x, y])

#Add the pd.Series with the coordinates in the good format in the merged dataframe
df_merged =df_merged.join(df_merged.apply(convertCoords, axis=1))

print('df_merged after join')
print(df_merged.head)

df_merged = df_merged.rename(columns={0: "latitude", 1: "longitude"}) #Rename column

print('df_merged after join renamed')
print(df_merged.head)

# Add columns color to the dataframe to set color attribute
class_color = {'A':'#922b21', 'B':'#f1c40f', 'C':'#27ae60', 'D':'#31f2cd', 'E': '#2980b9','F': '#7d3c98', 'W': '#34495e'}
df_merged['color'] = df_merged['classe_performance_globale'].replace(to_replace = class_color)

print(df_merged.head())

## Token for the map
mapbox_access_token = "pk.eyJ1IjoiY2VsaWVkcyIsImEiOiJja3djcDZpa24zd2V6Mm5ybzgzcDJnenI3In0.i1YF3PDr7eMc2Z56MR5E9g"


###########################################################################################################################
########################################################## Dashboard ######################################################
###########################################################################################################################


@app.callback(
    Output("page-content", "children"),
    [Input("url", "pathname")]
)
def render_page_content(pathname):
	if pathname == "/":
		return [
			html.H1('Nosocomial Disease Prevention In France',
					style={'textAlign':'center'}),
			html.Hr(),
			html.Div([
				dbc.Card([
					dbc.CardHeader(["What are nosocomial infections ?"], className='card-header-customized'),
					dbc.CardBody([
						html.H5(["Nosocomial infections are infections contracted during a stay in a health establishment (hospital, clinic, etc.). \
							The fight of health establishments against this type of infection is assessed through indicators, making possible \
							the visualisation and monitoring of the actions (organization, resources) taken to prevent them."], 
							style= {"text-align":"justify"}),	
					]),
					dbc.CardImg(src=app.get_asset_url("nosocomial_diseases.png"), bottom=True)
				], className ="card-customized", 
				style = {"margin-top": "12px", "margin-bottom":"12px"}),
			]),
			html.Div([
				dbc.Card([
					dbc.CardHeader(["Prevention in the French Health Establishments"], className = "card-header-customized"),
					dbc.CardBody([
						html.H5(["In France, nosocomial infections are believed to be responsible for about 4000 deaths each year.\
								To prevent them, the French government took several measures in accordance with the recommendations \
								of a committee of experts (ie: the National Technical Committee for Nosocomial Infections and Healthcare-Related Infections) for good prevention practices. \
								One of these measures consists of the attribution of a grade for each health establishment depicting the efforts made to prevent nosocomial infections. These grades rest on \
								several assessment criteria (ICALIN, SARM, ICSHA, SURVISO, ICATB) covering all good prevention practices."],
								style={"text-align":"justify"}),
						html.Hr(),
						html.H4(["This dashboard present the evolution of the different marks and criteria of assessment from 2007 to 2009."], 
							style={"text-align":"center"})

					]),
				], className = "card-customized", 
				style = {"margin-top": "12px", "margin-bottom":"12px"}),
			]),
			html.Div([
					dbc.Card([
						dbc.CardHeader(["Sources"], className="card-header-customized"),
						dbc.CardBody([
							dbc.Row([
								dbc.Col([
									html.Dl([
										html.Dt("The data concerning the establishments originate from the French Ministry of Solidarity and Health (data.gouv)."),
										html.Dd([
											html.A("French health establishments data", 
											href="https://www.data.gouv.fr/fr/datasets/les-indicateurs-relatifs-aux-infections-nosocomiales-dans-les-etablissements-de-sante-30378376/",
											target='_blank',
											style = {"color":"#c6c4c0"}
											)
										]),
									]),
									html.H6("The dataset of French establishments has been modified before use via R (v.4.1.1): "),
									html.Ul([
										html.Li("Reduction of the Excel file from 3 pages to 1, imported in '.csv' format"),
										html.Li("Recoding of variables")
									], style = {"margin-left":"20px"}),

									
									html.Hr(), 
									html.Button("Download the cleaned data", 
										id="btn-download-global-data", 
										className = "btn btn-secondary"),
									dcc.Download(id="download-data-global")
									

								],
								style={'border-right': 'solid 1px', 
									'border-color':'#EAEAE9',
									'padding-right': '10px', 
									'margin-top': '10px'},
								),
								dbc.Col([
									html.Dl([
										html.Dt("To locate the different establishments considerated, the 'Référentiel Finess' file has been downloaded.\
												It provides the connection between the Finess number of each establishments and their characteristics (including GPS coordinates)."),
										html.Dd([
											html.A("Connection finess number - establishments characterisitics",
													href= "https://www.data.gouv.fr/fr/datasets/referentiel-finess-t-finess/",
													target = '_blank', 
													style = {"color":"#c6c4c0"},
													)
										]),
									]),
									html.H6(["For time and memory reasons, this dataset has been manually shorten to retrieve only the finess \
											number and the GPS coordinates associated."], 
											style={"margin-bottom":"28px"}),
									
									html.Hr(),
									html.Button("Download the finess shortened file",
												id="btn-download-finess-data",
												className= "btn btn-secondary"), 
									dcc.Download(id="download-data-finess")


								], 
								style = {'margin-top': '10px'})
							]),
						]),
					], className="card-customized", 
					),
			], style = {"margin-top": "12px", "margin-bottom":"12px"}),
		]


	elif pathname == "/global":
		return [
				html.H1('Global overview of the different years',
						style={'textAlign':'center'}),
				html.Hr(),
				html.Div([
						dbc.Card([
							dbc.CardHeader("Evolution of the number of establishment assessed",
								class_name= "card-header-customized"
							),
							dbc.CardBody(
								dcc.Graph(id="nb_assess",
										figure = bar_nb_estab,
								),
							),
						],
						class_name = "card-customized",
						style = {"margin-top": "12px", "margin-bottom":"12px"})	
				]),



				html.Div([
						dbc.Card([
							dbc.CardHeader("Criteria of assessment : definition and evolution",
									class_name= "card-header-customized"
							),

							dbc.CardBody([
								dbc.Row([
									html.H3("Global Performance Classes",
										style={"margin-top": "10px", "margin-bottom":"10px"}),
									html.Hr(),
									dbc.Col([
										dcc.Checklist(
											id = "class_evolv_selected",
											options = [{'label': class_perf, 'value': class_perf} for class_perf in sorted(df.classe_performance_globale.unique())],
											value = ["A"],
											labelStyle = {'display': 'inline-block'},
											style={"margin-top": "10px", "margin-bottom":"10px"},
											#inputClassName = "btn-check",
											#labelClassName = "btn btn-primary",
											#className = "btn-group"
										),

										dash_table.DataTable(
											id='tbl', data= df_classe.to_dict('records'),
											columns= [{"name": i, "id": i} for i in df_classe.columns],
											style_table={'overflowX': 'auto',
														"margin-top": "40px", "margin-bottom":"40px"},
											style_cell={'height': 'auto', 
														'minWidth': '140px', 
														'width': '140px', 
														'maxWidth': '140px',
														'whiteSpace': 'normal', 
														'textAlign' : 'center',
														'border': '1px solid grey'}, 
											style_data={'color': 'black',
														'backgroundColor': 'white'},
											style_data_conditional=[
													{'if': {'row_index': 'odd'},
													'backgroundColor': '#F8F5F0'},
											],	
											style_header = { 
												'backgroundColor' : '#c6c4c0', 
												'fontWeight': 'bold',
												'color': '#353B38'},
		
										),
									]),

									dbc.Col([
										dbc.Card([
											dbc.CardBody([
												dcc.Graph(id="criteria_evolv"),
											],
											class_name = "card-plot-customized")
										]),

									]),


								]),

								
								dbc.Row([
									html.H3("Marks repartition by criteria",
										style={"margin-top": "10px", "margin-bottom":"10px"}),
									html.Hr(),
									dbc.Col([
										dcc.Dropdown( #Second Dropdown menu to select the indicator of interest
											id = "indicator", 
											options = [{"label" : "ICALIN", "value": "note_totale_ICALIN" }, 
											{"label" : "ICSHA", "value": "note_ICSHA" }, 
											{"label" : "ICATB", "value": "note_total_ICATB" },
											{"label" : "SARM", "value": "indice_SARM" }
											],
											value = "note_totale_ICALIN",
											searchable = False,
										),	

										html.P(
											id="indicator_def",
										),
									]),
									dbc.Col([
										dbc.Card([
											dbc.CardBody([
												dcc.Graph(id="global_histogram"),
											],
											class_name = "card-plot-customized")
										]),
									],
									width=8
									),
								]),
							]),
						],
						class_name = "card-customized",
						style = {"margin-top": "12px", "margin-bottom":"12px"},
					),
				]),

				html.Div([
					dbc.Card([
						dbc.CardHeader("Overview of the assessment of surgery services : SURVISO",
								class_name= "card-header-customized"),
						
						dbc.CardBody([
							dbc.Row([
								dbc.Col([
									dbc.Card([
											dbc.CardBody([
												dcc.Graph(id="surviso_graph", figure = surviso_graph,
												)
											],
											class_name = "card-plot-customized")
										]),
									],
									width = 8
								),

								dbc.Col([
									html.Br(),
									html.Br(),
									html.H5("SURVISO : Operator site infection monitoring", className="criteria-title"),
									html.Br(),
									html.P("SURVISO caracterizes the commitment of the establishment in a process of evaluation and improvement \
										of practices and control of the infectious risk in surgery. SURVISO indicates the proportion of the establishment's \
										surgical departments that were involved during the year in an epidemiological investigation. \
										It only concerns structures performing surgery.", 
										className="criteria-definition")
								]),


							dbc.Row([
								html.H3("Evolution of the proportion of the services assessed in each establishment by year",
									style={"margin-top":"35px", "margin-bottom": "25px"}),
								html.Hr(),
								dcc.Graph(id="prop_graph", figure = prop_graph,
										),
								html.P("NB: Select a part of a graph to zoom in",
									className = "text-secondary")
								]),
							])
						]),
					],
					class_name = "card-customized"),

				])
			]

	elif pathname == "/by-year":
		return [
				html.H1('Overview by Year',
						style={'textAlign':'center'}),

				html.Hr(),
				html.Div([
					dbc.Card([
						dbc.CardHeader("Select year of interest",class_name= "card-header-customized"),
						dbc.CardBody([
							dbc.Row([ 
								dcc.Dropdown( #First Dropdown menu to select the year of interest
									id = "year", 
		 							options = [{"label" : year, "value": year} for year in df.annee.unique()], #Display unique year available
		 							value = "2007",
		 							className = "dropdown",
		 							style={"margin-top": "10px", "margin-bottom":"10px"}
		 						),
		 					]),

							dbc.Row([
								dbc.Col([
									dbc.Card([
										html.P("Establishments considered:", className= "card-text"),
										html.H2(id="total_establissment_year", className="card-title")
										],
										body=True, 
										color = "dark",
										inverse = True, 
										style={"margin-top": "10px", "margin-bottom":"10px"}
									),
								]),

								dbc.Col([
									dbc.Card([
										html.P("Establishments fully assessed:",  className= "card-text"),
										html.H2(id="sum_all_assess", className="card-title")
										],
										body=True, 
										color = "dark",
										inverse = True,
										style={"margin-top": "10px", "margin-bottom":"10px"}
									),
								]),

								dbc.Col([
									dbc.Card([
										html.P("Average aggregate score of establishments:",  className= "card-text"),
										html.H2(id="mean_score_total", className="card-title")
										],
										body=True, 
										color = "light",
										style={"margin-top": "10px", "margin-bottom":"10px"}
									),
								]),

		 					]),
						],
						),
					],
					class_name = "card-customized",
					style = {"margin-top": "12px", "margin-bottom":"12px"}
					),
				]),

				html.Div([
					dbc.Card([
						dbc.CardHeader("Criteria of assessment : repartition and definition by year", class_name= "card-header-customized"),
						dbc.CardBody([
							dbc.Row([
								html.H3("Repartition of the global performance classes in France",
										style={"margin-top": "10px", "margin-bottom":"10px"}),
								html.Hr(),
								dbc.Col([
									dcc.Checklist(
										id = "class_selected",
										options = [{'label': class_perf, 'value': class_perf} for class_perf in sorted(df.classe_performance_globale.unique())],
										value = ["A","B","C"],
										style={"margin-top": "10px", "margin-bottom":"10px",'align-content':'center'},
										#inputClassName = "btn-check",
										#labelClassName = "btn btn-primary",
										#className = "btn-group", 
									),

									html.Div([
            							# Map-legend
            							html.Hr(),
            							html.Ul([
            								html.Span("", className='classe-a'),
            								html.Li("Classe A"),

            								html.Span("", className='classe-b'),
            								html.Li("Classe B"),

            								html.Span("", className='classe-c'),
            								html.Li("Classe C"),

            								html.Span("", className='classe-d'),
            								html.Li("Classe D"),

            								html.Span("", className='classe-e'),
            								html.Li("Classe E",),

            								html.Span("", className='classe-f'),
            								html.Li("Classe F"),

            								html.Span("", className='classe-w'),
            								html.Li("Classe W"),
            							],
            							className='legend'
            							),

            							html.Br(),

            							html.P("NB: Only the establishments whose GPS coordinates where shared are displayed.")
            						]),
								],
								style={'border-right': 'solid 1px', 
									'border-color':'#EAEAE9',
									'padding-right': '10px', 
									'align-content':'center', 
									'margin-bottom': '90px',
									'margin-top': '10px'},
								width = 3
								),

								dbc.Col([
									dcc.Graph(id='map_class',
											style={"margin-top": "10px", 
													"margin-bottom":"10px", 
													"align-content":'center'}, 
									),
								],
								),
							]),
							dbc.Row([
								html.H3("Marks repartition by criteria",
										style={"margin-bottom":"10px"}),
								html.Hr(),
								dbc.Col([
									dcc.Dropdown( #Second Dropdown menu to select the indicator of interest
										id = "indicator_by_year", 
										options = [{"label" : "ICALIN", "value": "note_totale_ICALIN" }, 
										{"label" : "ICSHA", "value": "note_ICSHA" }, 
										{"label" : "ICATB", "value": "note_total_ICATB" },
										{"label" : "SARM", "value": "indice_SARM" }, 
										],
										value = "note_totale_ICALIN",
										className = "dropdown",
										style={"margin-top": "10px", "margin-bottom":"10px"}	
									),
									html.P(
										id="indicator_def_year"),
								]),

								dbc.Col([
									dbc.Card([
										dbc.CardBody([
	 										dcc.Graph(id="year_histogram") #Graph for the indicators : ICALIN, ICSHA, ICATB
	 									],
	 									className= "card-plot-customized"),
	 								]),
	 							]),
							]),
						]),
					],
					class_name = "card-customized",
					style = {"margin-top": "12px", "margin-bottom":"12px"}),
				]),

				html.Div([
					dbc.Card([
						dbc.CardHeader("Overview of the assessment of surgery services : SURVISO", class_name= "card-header-customized"),
						dbc.CardBody([
							dbc.Row([
								html.Br(),
								html.Br(),
								html.H5("SURVISO : Operator site infection monitoring", className="criteria-title"),
								html.Br(),
								html.P("SURVISO caracterizes the commitment of the establishment in a process of evaluation and improvement \
										of practices and control of the infectious risk in surgery. SURVISO indicates the proportion of the establishment's \
										surgical departments that were involved during the year in an epidemiological investigation. \
										It only concerns structures performing surgery.", 
									className="criteria-definition", 
									),
							],
							style= {"margin-right": "20px", "margin-left": "10px"}),

							dbc.Row([
								dbc.Col([
									dbc.Card([
										dbc.CardBody([
											dcc.Graph(id="surviso_year"),
										],
											className= "card-plot-customized"
										),
									]),
								]),
								dbc.Col([
									dbc.Card([
										dbc.CardBody([
											dcc.Graph(id="completion_surviso"),
										],
											className= "card-plot-customized"
										),
									]),
								]),
							]),

						]),
					]),

				])
            ]
    # If the user tries to reach a different page, return a 404 message
	return [
			html.H1("404: Not found", className="text-danger"),
			html.Hr(),
			html.P(f"The pathname {pathname} was not recognised..."),
		]
	


###########################################################################################################################
################################################ Callbacks for dashbard functionnalities ##############################################
###########################################################################################################################



###################################### GLOBAL TAB CALLBACK ################################################################

@app.callback(Output("criteria_evolv", "figure"),
			 [Input("class_evolv_selected", "value")])

def final_mark_evolution(classes_selected):
	"""Callback that generates the graph representing the evolution of the mark per year"""

	#Data retrieval
	count_series = df.groupby(['annee', 'classe_performance_globale']).size() #Group by year an class and count number of each in class for each year
	count_frame = count_series.to_frame(name = 'nb').reset_index() #Conversion of the series to a dataframe : colnames = | annee | classe_performance_globale | nb |
	count_frame_criteria_selected = count_frame.loc[count_frame['classe_performance_globale'].isin(classes_selected)]

	#Plot preparation
	criteria_evolv = px.line(count_frame_criteria_selected, 
					x = "annee",
					y =  "nb",
					color = 'classe_performance_globale',
					title = 'Evolution of the overall performance class of establishments'
					)

	criteria_evolv.layout.update(legend_title = "Performance Classes" )
	return criteria_evolv


@app.callback(Output("global_histogram","figure"), 
			 [Input("indicator", "value")])

def histogram_indicators_global(indicator_name):
	"""Callback that generates the histogram global for the indicators selected by the user"""
	global_histogram = px.histogram(df, 
									x= indicator_name, 
									color="annee",
									title = "Evolution of the criteria assessed",
									labels = { indicator_name : 'Values', 'y': "Repartition"})

	global_histogram.update_layout(legend_title = "Years : ")
	return global_histogram


@app.callback(Output("indicator_def", "children"),
			[Input("indicator", "value")])

def display_consistent_def(indicator_name):
	"""Callback that display the criteria definition selected by the user in the dropdown menu"""
	if indicator_name == "note_totale_ICALIN":
		definition = (
					 html.Br(),
					 html.Br(),
					 html.H5("ICALIN : Composite Index of Nosocomial Infections Control Activities", className="criteria-title"),
					 html.Br(),
					 html.P("ICALIN aims to asssess the fight against nosocomial disease in the establishment, \
					 	the means it has mobilized and the actions it has implemented.", 
					 	className="criteria-definition"))

	elif indicator_name == "note_ICSHA":
		definition = (html.Br(),
					  html.Br(),
					  html.H5("ICSHA : Indicator of consumption of hydro-alcoholic solutions", className="criteria-title"),
					  html.Br(),
					  html.P("ICSHA is an indirect marker of the effective implementation of hand hygiene \
					  	by healthcare professionals and the monitoring of recommendations for preventive practices.\
					  	It is expressed as a percentage.", className="criteria-definition"))

	elif indicator_name == "indice_SARM":
		definition = (html.Br(),
					  html.Br(),
					  html.H5("SARM : Methicillin-resistant Staphylococcus aureus index", className="criteria-title"),
					  html.Br(),
					  html.P("The SARM index depends on the number of patients coming from another hospital (imported SARM), \
					  	the prevention of the spread of SARM from one patient to another (SARM acquired in the establishment) \
					  	and the prescribing policy antibiotics. It reflects the microbial ecology of the establishment and its \
					  	ability to control it through prevention and control of antibiotic prescriptions.", 
					className="criteria-definition"))

	else : #Indicator name =ICATB
		definition = (html.Br(),
					  html.Br(),
					  html.H5("ICATB: Indicator of the correct use of antibiotics", className="criteria-title"), 
					  html.Br(),
					  html.P("ICATB characterizes the organization of the establishment relating to the proper use of antibiotics, \
							the resources mobilized and the actions implemented. It describes the individual benefit \
							for the patient (best treatment available) and collective (limitation of the emergence of resistant bacteria)",
					className="criteria-definition"))

	return definition



#################################################### BY YEAR TAB CALLBACKS ########################################################

@app.callback(Output("total_establissment_year", "children"),
			  [Input("year", "value")])

def display_nb_establissement(year_selected) :
	"""Callback that counts the number of establissment assessed each year"""
	total_estab = len(df.loc[df["annee"].isin([year_selected])])
	return total_estab

@app.callback(Output("mean_score_total", "children"), 
			 [Input("year", "value")])

def mean_final_score(year_selected):
	"""Callback that calculated the mean of the aggregated score of all the establissment per year"""
	df_year = df.loc[df["annee"].isin([year_selected])]
	mean_score = df_year.score_agrege.sum()/len(df_year.score_agrege)
	return round(mean_score, 2)


@app.callback(Output("sum_all_assess", "children"), 
			  [Input("year", "value")])

def nb_completely_assessed(year_selected):
	"""Callback that count the number of establissement assessed with all the criteria"""
	df_year = df.loc[df["annee"].isin([year_selected])]
	df_completely_assessed = df_year.loc[df_year.note_totale_ICALIN.notna() & #4 first indicators not containing any NA value
       									df_year.note_total_ICATB.notna() & 
       									df_year.note_ICSHA.notna() & 
      									df_year.indice_SARM.notna() &
      									(df_year["enquete_SURVISO"] == "1") & (df_year["part_service_chi_SURVISO"]==1) | #Surviso is correctly assessed if all the service have been assessed
      									(df_year["enquete_SURVISO"] == "2")] #All the structure are not concerned with Surviso
	nb_com_assessed = len(df_completely_assessed)
	print(nb_com_assessed)
	return nb_com_assessed



@app.callback(Output("year_histogram","figure"), 
			 [Input("year","value"), Input("indicator_by_year", "value")])

def histogram_indicators_by_year(year_selected, indicator_name):
	"""Callback that generates the histogram global or by year for the indicators selected by the user"""
	df_year = df.loc[df['annee'].isin([year_selected])] #Selection of the row corresponding to the year selected by the user
	year_histogram = px.histogram(df_year, x= indicator_name) #Histogram for the indicator selected
	return year_histogram

@app.callback(Output("indicator_def_year", "children"),
			[Input("indicator_by_year", "value")])

def display_consistent_def(indicator_name):
	"""Callback that display the criteria definition selected by the user in the dropdown menu"""
	if indicator_name == "note_totale_ICALIN":
		definition = (
					 html.Br(),
					 html.Br(),
					 html.H5("ICALIN : Composite Index of Nosocomial Infections Control Activities", className="criteria-title"),
					 html.Br(),
					 html.P("ICALIN aims to asssess the fight against nosocomial disease in the establishment, \
					 	the means it has mobilized and the actions it has implemented.", 
					 	className="criteria-definition"))

	elif indicator_name == "note_ICSHA":
		definition = (html.Br(),
					  html.Br(),
					  html.H5("ICSHA : Indicator of consumption of hydro-alcoholic solutions", className="criteria-title"),
					  html.Br(),
					  html.P("ICSHA is an indirect marker of the effective implementation of hand hygiene \
					  	by healthcare professionals and the monitoring of recommendations for preventive practices.\
					  	It is expressed as a percentage.", className="criteria-definition"))

	elif indicator_name == "indice_SARM":
		definition = (html.Br(),
					  html.Br(),
					  html.H5("SARM : Methicillin-resistant Staphylococcus aureus index", className="criteria-title"),
					  html.Br(),
					  html.P("The SARM index depends on the number of patients coming from another hospital (imported SARM), \
					  	the prevention of the spread of SARM from one patient to another (SARM acquired in the establishment) \
					  	and the prescribing policy antibiotics. It reflects the microbial ecology of the establishment and its \
					  	ability to control it through prevention and control of antibiotic prescriptions.", 
					className="criteria-definition"))

	else : #Indicator name =ICATB
		definition = (html.Br(),
					  html.Br(),
					  html.H5("ICATB: Indicator of the correct use of antibiotics", className="criteria-title"), 
					  html.Br(),
					  html.P("ICATB characterizes the organization of the establishment relating to the proper use of antibiotics, \
							the resources mobilized and the actions implemented. It describes the individual benefit \
							for the patient (best treatment available) and collective (limitation of the emergence of resistant bacteria)",
					className="criteria-definition"))

	return definition


@app.callback(Output("map_class", "figure"),
			  [Input("class_selected","value"),
			  Input("year","value")])

def display_map(class_selected, year):
	"""Callback that generates the map with the performance class in France"""

	df_year = df_merged.loc[df_merged['annee'].isin([year])] #Subset df according to year selected
	df_year_class = df_year.loc[df_year['classe_performance_globale'].isin(list(class_selected))] #Subset df according to class selected
	df_map = df_year_class[df_year_class['latitude'].notna() & df_year_class['longitude'].notna()] #Eliminate the line where coordinates are not available
	
	map_class = go.Figure()

	map_class.add_trace(
		go.Scattermapbox(
	        lat=df_map['latitude'],
	        lon=df_map['longitude'],
	        mode='markers',
	        hovertext = df_map['classe_performance_globale'],
	        marker=go.scattermapbox.Marker(
	            size=8,
	            color=df_map['color'],
	            opacity=0.7,
	        )
	    )
    )

	map_class.update_layout(
	    autosize=True,
	    uirevision= 'foo',
	    hovermode='closest',
	    height = 800,
	    mapbox=dict(
	        accesstoken=mapbox_access_token,
	        bearing=0,
	        center=dict(
	            lat= 48.866669,
	            lon=2.33333
	        ),
	        pitch=0,
	        zoom=6
	    	)
	)
		
	return map_class

@app.callback(Output("surviso_year", "figure"), 
			 [Input("year","value")])

def pie_surviso(year_selected):
	"""Callback that generates the SURVISO pie by year"""
	count_surviso = df.groupby(['annee', 'enquete_SURVISO']).size() #Group by year an class and count number of each in class for each year
	surviso_frame = count_surviso.to_frame(name = 'surviso_nb').reset_index() #

	surviso_graph = px.pie(surviso_frame,
						values = "surviso_nb",
						names="enquete_SURVISO",
						title = "Distribution of the data linked to SURVISO status",
						labels = {"1" : "Assessment carried out", "0":"Assessment not carried out","2": "Establishment not concerned"}
						)

	surviso_graph.layout.update(legend_title = "Establishment status" )
	surviso_graph.update_layout(legend=dict(yanchor='top', y=-0.1, xanchor="left", x=0.01))
	surviso_graph = newLegend(surviso_graph, {"1" : "Assessment carried out", "0":"Assessment not carried out","2": "Establishment not concerned"})
	return surviso_graph

@app.callback(Output("completion_surviso", "figure"), 
			  [Input("year", "value")])

def proportion_surviso(year_selected):
	df_year = df.loc[df["annee"].isin([year_selected])]
	surviso_prop = px.histogram(df_year, x="part_service_chi_SURVISO",
								title = "Proportion of the services assessed in each establishment", 
								labels= {"part_service_chi_SURVISO":"SURVISO proportions", "count":"Count"})
	return surviso_prop

@app.callback(Output("download-data-global", "data"),
			[Input("btn-download-global-data", "n_clicks")],
			prevent_initial_call=True,)

def func(n_clicks):
    return dcc.send_data_frame(df_merged.to_csv, "data_maladies_nosocomiales_cleaned.csv")


@app.callback(Output("download-data-finess", "data"), 
			 [Input("btn-download-finess-data", "n_clicks")], 
			 prevent_initial_call=True)

def func(n_clicks):
    return dcc.send_data_frame(df_finess.to_csv,"t-finess_modified.csv")

	

### Running the app in an infinite loop
if __name__ == '__main__':
     app.run_server(debug=True)
    